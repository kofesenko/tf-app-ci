terraform {
  backend "s3" {
    bucket         = "my-tf-state-bucket-rndcharskf"
    key            = "staging/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "terraform-state-locking"
    encrypt        = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
provider "aws" {
  region  = var.region
}

module "vpc" {
  source = "./modules/vpc"

  environment_name = var.environment_name
  vpc_cidr         = var.vpc_cidr
  azs              = var.azs
  private_subnets  = var.private_subnets
  public_subnets   = var.public_subnets

  additional_tags = {
    Environment = "Staging"
    Owner       = "KF"
  }
}

module "alb" {
  source = "./modules/alb"

  private_subnets_id   = module.vpc.private_subnets_id
  public_subnets_id    = module.vpc.public_subnets_id
  vpc_id               = module.vpc.vpc_id
  sg_alb_ingress_ports = var.sg_alb_ingress_ports
  depends_on           = [module.vpc]
}

module "asg" {
  source = "./modules/asg"

  environment_name     = var.environment_name
  private_subnets_id   = module.vpc.private_subnets_id
  public_subnets_id    = module.vpc.public_subnets_id
  vpc_id               = module.vpc.vpc_id
  target_group_arns    = module.alb.target_group_arns
  alb_sg               = module.alb.alb_sg
  sg_asg_ingress_ports = var.sg_asg_ingress_ports
  instance_type        = var.instance_type
  depends_on           = [module.vpc, module.alb]
}

module "ecr" {
  source = "./modules/ecr"
}

module "ecs" {
  source = "./modules/ecs"

  ACCOUNT_ID        = var.ACCOUNT_ID
  ecr_url           = module.ecr.ecr_url
  target_group_arns = module.alb.target_group_arns
  depends_on        = [module.ecr, module.vpc]
}